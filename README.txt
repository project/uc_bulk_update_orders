Description:
Provides admin page for updating status of orders in bulk using Drupal batch process.
Order status, order comment, admin comment and send email notification can be set for 
all selected orders.

Requirements:
Drupal 6.x
Ubercart 2.x

Installation:
1. Copy the extracted uc_bulk_update_orders directory to your Drupal sites/all/modules directory.
2. Login as an administrator. Enable the module at the http://www.example.com/?q=admin/build/modules
3. This is optional, configure permission "bulk status update orders" at http://www.example.com/?q=admin/user/permissions
4. Bulk status update orders form can be found at http://www.example.com/?q=admin/store/orders/bulk_update

Support:
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/uc_bulk_update_orders